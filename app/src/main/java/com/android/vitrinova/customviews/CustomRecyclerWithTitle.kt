package com.android.vitrinova.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.widget.LinearLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.android.vitrinova.R
import com.android.vitrinova.databinding.CustomReyclerviewBinding
import com.android.vitrinova.utility.extension.clickListener
import com.android.vitrinova.utility.extension.gone


class CustomRecyclerWithTitle @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = 0
) : LinearLayout(context, attrs, defStyleAttr) {


    var binding: CustomReyclerviewBinding =
        CustomReyclerviewBinding.inflate(LayoutInflater.from(context), this, true)


    init {
        val typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomRecyclerWithTitle)


        with(typedArray) {


            val itemType = getInteger(R.styleable.CustomRecyclerWithTitle_itemType, 0)

            setType(itemType)

            recycle()
        }
    }

    private fun setType(itemType: Int) {
        when (itemType) {
            1 -> {
                binding.customRvAllItemTv.gone()
            }
            2 -> {
                binding.customRvTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
                binding.customRvAllItemTv.apply {
                    setTextColor(ContextCompat.getColor(context, R.color.white))
                    compoundDrawables.forEach {
                        it?.setTint(
                            ContextCompat.getColor(
                                context,
                                R.color.white
                            )
                        )
                    }
                }

            }
        }
    }

    val recyclerView: RecyclerView
        get() = binding.customRv


    fun setText(text: String) {
        binding.customRvTitle.text = text.toUpperCase()

    }

    fun allItemClickListener(onclick: (view: View) -> Unit) {
        binding.customRvAllItemTv.clickListener {
            onclick(it)
        }
    }

}