package com.android.vitrinova.utility.extension

import android.content.res.Resources
import android.graphics.Paint
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

val Int.px: Int
    get() = (this / Resources.getSystem().displayMetrics.density).toInt()

val Int.dp: Int
    get() = (this * Resources.getSystem().displayMetrics.density).toInt()

fun View.show() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.margin(top: Int? = null, left: Int? = null, bottom: Int? = null, right: Int? = null) {

    val param = this.layoutParams as ViewGroup.MarginLayoutParams

    top?.let {
        param.topMargin = it
    }
    left?.let {
        param.leftMargin = it
    }
    bottom?.let {
        param.bottomMargin = it
    }
    right?.let {
        param.rightMargin = it
    }

    this.layoutParams = param

}

fun TextView.centerLine() {
    this.paintFlags = this.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}