package com.android.vitrinova.utility.enums

enum class DiscoverTypes(var value: String, var type: Int) {
    FEATURED("featured", 1),
    NEW_PRODUCTS("new_products", 2),
    CATEGORIES("categories", 3),
    COLLECTIONS("collections", 4),
    EDITOR_SHOPS("editor_shops", 5),
    NEW_SHOPS("new_shops", 6)
}