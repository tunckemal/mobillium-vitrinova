package com.android.vitrinova.utility

import android.view.View
import androidx.viewpager.widget.ViewPager

class FadePageTransformer : ViewPager.PageTransformer {
    override fun transformPage(
        page: View,
        position: Float
    ) {
        val pageWidth = page.width
        when {
            position <= 0 -> {
                page.translationX = 0f
                page.translationZ = 20f

            }
            position <= 1 -> {
                page.translationX = pageWidth * -position
                page.translationZ = 20f - position
            }

        }
    }
}