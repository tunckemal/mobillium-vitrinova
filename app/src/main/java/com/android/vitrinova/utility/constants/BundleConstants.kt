package com.android.vitrinova.utility.constants

class BundleConstants {

    companion object {
        const val POST_TYPE = "postType"
        const val ITEMS = "item"
        const val TITLE = "title"
    }
}
