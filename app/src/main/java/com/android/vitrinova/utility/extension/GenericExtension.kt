package com.android.vitrinova.utility.extension

import android.util.Log

fun String.Logd(message: String) {
    Log.d(this, message)
}

