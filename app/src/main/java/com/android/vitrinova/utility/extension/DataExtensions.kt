package com.android.vitrinova.utility.extension


fun Int.toPrice(): String {
    return "$this TL"
}

fun String.firstCharacter(): String {
    return this.take(1).toUpperCase()
}