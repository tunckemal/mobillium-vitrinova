package com.android.vitrinova.utility.databinding

import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.android.vitrinova.utility.extension.loadWithCache


@BindingAdapter("imageUrl")
fun loadImage(view: ImageView, url: String?) {
    view.loadWithCache(url ?: "")
}