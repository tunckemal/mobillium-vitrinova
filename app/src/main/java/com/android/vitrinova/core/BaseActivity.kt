package com.android.vitrinova.core


import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.os.PersistableBundle
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.ViewDataBinding
import com.android.vitrinova.R
import com.android.vitrinova.utility.Loading


abstract class BaseActivity<ViewModel : BaseViewModel, ViewBinding : ViewDataBinding> : AppCompatActivity() {

    @get:LayoutRes
    protected abstract val layoutRes: Int
    protected abstract fun getViewModelClass(): Class<ViewModel>

    private lateinit var loading: Dialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutRes)

        initUI()
    }

    open fun initUI(){}

    open fun showMessage(message: String) {
        Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT).show()
    }

    open fun showMessage(messageId: Int) {
        Toast.makeText(applicationContext, getString(messageId), Toast.LENGTH_SHORT).show()
    }


    open fun showLoading() {
        if (!this::loading.isInitialized) {
            loading = Loading.showLoadingDialog(this)
        } else {
            if (!loading.isShowing) {
                loading.show()
            }
        }
    }

    open fun hideLoading() {
        if (this::loading.isInitialized) {
            loading.dismiss()
            loading.cancel()
        }
    }

}