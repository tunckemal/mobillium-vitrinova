package com.android.vitrinova.core

import androidx.annotation.LayoutRes

abstract class BaseEntity(@LayoutRes resId: Int)