package com.android.vitrinova.core

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.android.vitrinova.data.source.network.exception.ApiErrorHandle
import com.android.vitrinova.data.source.network.exception.ErrorModel
import com.android.vitrinova.utility.LiveData.Event
import com.android.vitrinova.utility.LiveData.SingleLiveEvent
import com.android.vitrinova.utility.LiveData.event
import kotlinx.coroutines.cancel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow


abstract class BaseViewModel : ViewModel() {

    val errorHandle =
        SingleLiveEvent<Event<ErrorModel>>()
    val showLoading =
        SingleLiveEvent<Event<Boolean>>()
    val hideLoading =
        SingleLiveEvent<Event<Boolean>>()


    fun <T> execute(call: suspend () -> T, autoLoading: Boolean = true): Flow<DataState<T>> = flow {
        emit(DataState.Loading)
        if (autoLoading) {
            showLoading.value = true.event()
        }
        try {
            val result = call.invoke()
            emit(DataState.Success(result))

            if (autoLoading) {
                hideLoading.value = true.event()
            }

        } catch (e: Exception) {
            val error = ApiErrorHandle().traceErrorException(e)
            errorHandle.value = error.event()
            emit(DataState.Error(error))
            hideLoading.value = true.event()
        }
    }


    override fun onCleared() {
        super.onCleared()
        viewModelScope.cancel()
    }

}