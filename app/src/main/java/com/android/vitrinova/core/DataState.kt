package com.android.vitrinova.core

import com.android.vitrinova.data.source.network.exception.ErrorModel


sealed class DataState<out R> {
    data class Success<out T>(val data: T) : DataState<T>()
    data class Error(val exception: ErrorModel) : DataState<Nothing>()
    object Loading : DataState<Nothing>()
}

inline fun <T> DataState<T>.handle(
    loading: () -> Unit = {},
    success: (data: T) -> Unit = {},
    error: (error: ErrorModel) -> Unit = {}
) {
    when (this) {
        is DataState.Loading -> {
            loading()
        }
        is DataState.Success -> {
            success(this.data)
        }

        is DataState.Error -> {
            error(this.exception)
        }

    }
}