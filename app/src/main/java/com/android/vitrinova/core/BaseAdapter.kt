package com.android.vitrinova.core

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.android.vitrinova.R
import kotlin.properties.Delegates


abstract class BaseAdapter<Data> :
    ListAdapter<Data, BaseViewHolder<Data, *>>(DiffCallback<Data>()) {


    private var onItemClick: ((Data) -> Unit) = {}

    private var onViewClick: ((Data, View) -> Unit) = { _, _ -> }


    var items: List<Data> by Delegates.observable(emptyList()) { prop, old, new ->
        this.submitList(new)
    }


    abstract override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<Data, *>


    override fun onBindViewHolder(holder: BaseViewHolder<Data, *>, position: Int) {
        val animation = AnimationUtils.loadAnimation(holder.context, R.anim.cell_animation)
        holder.itemView.startAnimation(animation)
        holder.setOnViewClick(onViewClick).bindItem(getItem(position), onItemClick)
    }

    override fun onViewDetachedFromWindow(holder: BaseViewHolder<Data, *>) {
        super.onViewDetachedFromWindow(holder)
        holder.itemView.clearAnimation()
    }

    fun onItemClick(onClick: ((Data) -> Unit)): BaseAdapter<Data> {
        this.onItemClick = onClick
        return this
    }


    fun onViewClick(onClick: ((Data, View) -> Unit)): BaseAdapter<Data> {
        this.onViewClick = onClick
        return this
    }


    class DiffCallback<Data> : DiffUtil.ItemCallback<Data>() {
        override fun areItemsTheSame(oldItem: Data, newItem: Data) =
            oldItem.hashCode() == newItem.hashCode()

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Data, newItem: Data) = oldItem == newItem
    }

}