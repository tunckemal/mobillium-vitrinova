package com.android.vitrinova.presentation.adapter

import android.view.ViewGroup
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseAdapter
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.Shop
import com.android.vitrinova.databinding.CellShopsBinding


class ShopAdapter(private val fullscreen: Boolean = false) : BaseAdapter<Shop>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Shop, *> {
        return ShopViewHolder(parent,fullscreen)
    }

    class ShopViewHolder(parent: ViewGroup,private val fullscreen: Boolean = false) :
        BaseViewHolder<Shop, CellShopsBinding>(parent, R.layout.cell_shops) {

        override fun bind(item: Shop, binding: CellShopsBinding) {
            with(binding) {
                data = item
                executePendingBindings()
            }

            if (fullscreen) {
                binding.root.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            }
        }

    }
}