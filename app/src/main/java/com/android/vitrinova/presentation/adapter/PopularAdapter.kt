package com.android.vitrinova.presentation.adapter

import android.view.ViewGroup
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseAdapter
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.PopularProduct
import com.android.vitrinova.databinding.CellMiniProductBinding


class PopularAdapter : BaseAdapter<PopularProduct>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<PopularProduct, *> {
        return PopularViewHolder(parent)
    }

    class PopularViewHolder(parent: ViewGroup) :
        BaseViewHolder<PopularProduct, CellMiniProductBinding>(parent, R.layout.cell_mini_product) {

        override fun bind(item: PopularProduct, binding: CellMiniProductBinding) {
            with(binding) {
                data = item
                executePendingBindings()
            }
        }

    }
}