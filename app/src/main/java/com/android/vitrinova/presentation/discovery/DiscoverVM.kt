package com.android.vitrinova.presentation.discovery

import androidx.hilt.Assisted
import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.android.vitrinova.core.BaseViewModel
import com.android.vitrinova.core.DataState
import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.domain.usecases.DiscoverUseCases
import com.android.vitrinova.utility.LiveData.Event
import com.android.vitrinova.utility.LiveData.SingleLiveEvent
import com.android.vitrinova.utility.LiveData.event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach


@ExperimentalCoroutinesApi
class DiscoverVM @ViewModelInject constructor(
    private val useCases: DiscoverUseCases,
    @Assisted private val savedStateHandle: SavedStateHandle
) : BaseViewModel() {

    private val _discover = SingleLiveEvent<Event<DataState<ArrayList<MainDiscoverResponse>>>>()

    val discover: SingleLiveEvent<Event<DataState<ArrayList<MainDiscoverResponse>>>>
        get() = _discover

    fun setStateEvent(state: DiscoveryStateEvent) {

        when (state) {
            is DiscoveryStateEvent.FetchData -> {
                execute({ useCases.getDiscover() }).onEach {
                    _discover.value = it.event()
                }.launchIn(viewModelScope)
            }
        }

    }

}

sealed class DiscoveryStateEvent {

    object Initial : DiscoveryStateEvent()

    object FetchData : DiscoveryStateEvent()

    object FetchedData : DiscoveryStateEvent()

    object None : DiscoveryStateEvent()

}