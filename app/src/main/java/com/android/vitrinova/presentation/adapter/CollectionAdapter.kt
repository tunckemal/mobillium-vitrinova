package com.android.vitrinova.presentation.adapter

import android.view.ViewGroup
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseAdapter
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.Collection
import com.android.vitrinova.data.model.response.Product
import com.android.vitrinova.databinding.CellCollectionBinding
import com.android.vitrinova.databinding.CellProductBinding


class CollectionAdapter(private val fullscreen: Boolean = false) : BaseAdapter<Collection>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Collection, *> {
        return ProductViewHolder(parent,fullscreen)
    }

    class ProductViewHolder(parent: ViewGroup,private val fullscreen: Boolean = false) :
        BaseViewHolder<Collection, CellCollectionBinding>(parent, R.layout.cell_collection) {

        override fun bind(item: Collection, binding: CellCollectionBinding) {
            with(binding) {
                data = item
                executePendingBindings()
            }

            if (fullscreen) {
                binding.root.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            }
        }

    }
}