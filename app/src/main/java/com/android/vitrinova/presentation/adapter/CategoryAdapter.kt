package com.android.vitrinova.presentation.adapter

import android.view.ViewGroup
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseAdapter
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.Category
import com.android.vitrinova.databinding.CellCategoryBinding

class CategoryAdapter : BaseAdapter<Category>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Category, *> {
        return CategoryViewHolder(parent)
    }

    class CategoryViewHolder(parent: ViewGroup) :
        BaseViewHolder<Category, CellCategoryBinding>(parent, R.layout.cell_category) {

        override fun bind(item: Category, binding: CellCategoryBinding) {
            with(binding) {
                data = item
                executePendingBindings()
            }
        }

    }
}