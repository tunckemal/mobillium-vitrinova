package com.android.vitrinova.presentation.splash

import android.os.Handler
import android.os.Looper
import androidx.hilt.lifecycle.ViewModelInject
import com.android.vitrinova.core.BaseViewModel
import com.android.vitrinova.utility.LiveData.Event
import com.android.vitrinova.utility.LiveData.SingleLiveEvent
import com.android.vitrinova.utility.LiveData.event
import com.android.vitrinova.utility.constants.SPLASH_DELAY

class SplashVM @ViewModelInject constructor() : BaseViewModel() {

    val isLogin = SingleLiveEvent<Event<Boolean>>()

    fun startApp() {

        Handler(Looper.getMainLooper()).postDelayed({
            isLogin.value = true.event()
        }, SPLASH_DELAY)
    }
}