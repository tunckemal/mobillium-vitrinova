package com.android.vitrinova.presentation.detail

import android.os.Bundle
import com.android.easynav.src.Navigator
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseFragment
import com.android.vitrinova.data.model.response.Collection
import com.android.vitrinova.data.model.response.Product
import com.android.vitrinova.data.model.response.Shop
import com.android.vitrinova.databinding.FragmentDetailBinding
import com.android.vitrinova.presentation.adapter.CollectionAdapter
import com.android.vitrinova.presentation.adapter.EditorAdapter
import com.android.vitrinova.presentation.adapter.ProductAdapter
import com.android.vitrinova.presentation.adapter.ShopAdapter
import com.android.vitrinova.utility.constants.BundleConstants
import com.android.vitrinova.utility.enums.DiscoverTypes
import com.android.vitrinova.utility.extension.grid
import com.android.vitrinova.utility.extension.vertical
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class DetailFragment : BaseFragment<DetailVM, FragmentDetailBinding>() {

    override val layoutRes = R.layout.fragment_detail
    override fun getViewModelClass() = DetailVM::class.java

    private var postType = ""
    private var title = ""


    private val productAdapter by lazy { ProductAdapter(true) }
    private val shopAdapter by lazy { ShopAdapter(true) }
    private val editorAdapter by lazy { EditorAdapter(true) }
    private val collectionAdapter by lazy { CollectionAdapter(true) }

    override fun define() {
        super.define()

        postType = arguments?.getString(BundleConstants.POST_TYPE) ?: ""
        title = arguments?.getString(BundleConstants.TITLE) ?: ""

    }

    override fun initUI() {
        super.initUI()

        binding.fragmentDetailTitleTv.text = title

        setScreen()

    }

    override fun clickListeners() {
        super.clickListeners()

        binding.fragmentDetailBackButton.setOnClickListener {
            Navigator.navigateUp()
        }
    }

    private fun setScreen() {
        when (postType) {
            DiscoverTypes.NEW_PRODUCTS.value -> {
                initProductScreen()
            }
            DiscoverTypes.COLLECTIONS.value -> {
                initCollectionsScreen()
            }
            DiscoverTypes.EDITOR_SHOPS.value -> {
                initEditorScreen()
            }
            DiscoverTypes.NEW_SHOPS.value -> {
                initShopScreen()
            }

        }
    }

    private fun initProductScreen() {
        val items = arguments?.getParcelableArrayList<Product>(BundleConstants.ITEMS)

        binding.fragmentDetailRv.grid(productAdapter, column = 2)

        productAdapter.items = items ?: emptyList()

    }


    private fun initCollectionsScreen() {
        val items = arguments?.getParcelableArrayList<Collection>(BundleConstants.ITEMS)

        binding.fragmentDetailRv.vertical(collectionAdapter)

        collectionAdapter.items = items ?: emptyList()
    }

    private fun initEditorScreen() {
        val items = arguments?.getParcelableArrayList<Shop>(BundleConstants.ITEMS)

        binding.fragmentDetailRv.vertical(editorAdapter)

        editorAdapter.items = items ?: emptyList()
    }


    private fun initShopScreen() {
        val items = arguments?.getParcelableArrayList<Shop>(BundleConstants.ITEMS)

        binding.fragmentDetailRv.vertical(shopAdapter)

        shopAdapter.items = items ?: emptyList()
    }



    companion object {
        fun newInstance(bundle: Bundle): DetailFragment {

            val detailFragment = DetailFragment()
            detailFragment.arguments = bundle

            return detailFragment
        }
    }


}
