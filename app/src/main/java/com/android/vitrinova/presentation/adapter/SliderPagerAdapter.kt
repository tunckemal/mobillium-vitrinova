package com.android.vitrinova.presentation.adapter

import android.app.Service
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.android.vitrinova.R
import com.android.vitrinova.data.model.response.Featured
import com.android.vitrinova.databinding.CellSliderItemBinding

class SliderPagerAdapter(
    private val context: Context,
    private val items: List<Featured>
) : PagerAdapter() {


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        val layoutInflater =
            context.getSystemService(Service.LAYOUT_INFLATER_SERVICE) as LayoutInflater

        val binding = DataBindingUtil.inflate<CellSliderItemBinding>(
            layoutInflater,
            R.layout.cell_slider_item,
            null,
            false
        )

        binding.data = items[position]


        container.addView(binding.root)

        return binding.root
    }

    override fun getCount() = items.size


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }


}