package com.android.vitrinova.presentation.adapter.holders

import android.view.ViewGroup
import androidx.recyclerview.widget.LinearSnapHelper
import androidx.recyclerview.widget.RecyclerView
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.data.model.response.Shop
import com.android.vitrinova.databinding.CellMainEditorBinding
import com.android.vitrinova.presentation.adapter.EditorAdapter
import com.android.vitrinova.presentation.callbacks.DiscoverCallback
import com.android.vitrinova.utility.extension.horizantal
import com.android.vitrinova.utility.extension.position


class EditorMainHolder(
    parent: ViewGroup,
    private val discoverCallback: DiscoverCallback,
    private val viewPool: RecyclerView.RecycledViewPool
) : BaseViewHolder<MainDiscoverResponse, CellMainEditorBinding>(
    parent,
    R.layout.cell_main_editor
) {
    private val adapter by lazy { EditorAdapter() }
    override fun bind(item: MainDiscoverResponse, binding: CellMainEditorBinding) {

        with(binding) {

            item.shops?.get(0)?.let {
                binding.data = it
            }

            cellMainEditorRv.apply {
                setText(item.title ?: "")
                recyclerView.horizantal(adapter, snapHelper = LinearSnapHelper())

                recyclerView.position {
                    if (it != null && it > -1) {
                        item.shops?.get(it)?.let { shop ->
                            binding.data = shop
                        }
                    }
                }

                allItemClickListener {
                    discoverCallback.clickAllItem(item.type?:"")
                }

           //     recyclerView.setRecycledViewPool(viewPool)

            }




            adapter.items = item.shops ?: emptyList()
        }

    }


}