package com.android.vitrinova.presentation.adapter.holders

import android.view.ViewGroup
import android.view.animation.AccelerateInterpolator
import androidx.viewpager.widget.ViewPager
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.customviews.FixedSpeedScroller
import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.databinding.CellSliderBinding
import com.android.vitrinova.presentation.adapter.SliderPagerAdapter
import com.android.vitrinova.presentation.callbacks.DiscoverCallback
import com.android.vitrinova.utility.FadePageTransformer
import java.lang.reflect.Field


class SliderHolder(
    parent: ViewGroup,
    private val discoverCallback: DiscoverCallback
) : BaseViewHolder<MainDiscoverResponse, CellSliderBinding>(parent, R.layout.cell_slider) {
    override fun bind(item: MainDiscoverResponse, binding: CellSliderBinding) {

        binding.sliderViewPager.apply {
            adapter = SliderPagerAdapter(context, item.featured ?: emptyList())
            setPageTransformer(true, FadePageTransformer())

        }

        try {
            val mScroller: Field = ViewPager::class.java.getDeclaredField("mScroller")
            val sInterpolator = AccelerateInterpolator()
            mScroller.isAccessible = true
            val scroller = FixedSpeedScroller(context, sInterpolator)
            mScroller.set(binding.sliderViewPager, scroller)
        } catch (e: NoSuchFieldException) {
        } catch (e: IllegalArgumentException) {
        } catch (e: IllegalAccessException) {
        }

    }

}