package com.android.vitrinova.presentation.splash

import com.android.easynav.src.Navigator
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseFragment
import com.android.vitrinova.core.MainController
import com.android.vitrinova.databinding.FragmentSplashBinding
import com.android.vitrinova.presentation.tab.TabFragment
import com.android.vitrinova.utility.LiveData.EventObserver
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@AndroidEntryPoint
@ExperimentalCoroutinesApi
class SplashFragment : BaseFragment<SplashVM, FragmentSplashBinding>() {

    override val layoutRes = R.layout.fragment_splash

    override fun getViewModelClass() = SplashVM::class.java

    override fun initUI() {
        super.initUI()

        viewModel.startApp()
    }


    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.isLogin.observe(viewLifecycleOwner, EventObserver {
            Navigator.find<MainController>(TabFragment.newInstance()) {
                history = false
            }.navigate()
        })
    }

    companion object {
        fun newInstance() = SplashFragment()
    }


}
