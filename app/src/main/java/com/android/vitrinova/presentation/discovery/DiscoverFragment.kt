package com.android.vitrinova.presentation.discovery

import android.os.Bundle
import com.android.easynav.src.Navigator
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseFragment
import com.android.vitrinova.core.MainController
import com.android.vitrinova.core.handle
import com.android.vitrinova.databinding.FragmentDiscoverBinding
import com.android.vitrinova.presentation.adapter.DiscoverAdapter
import com.android.vitrinova.presentation.callbacks.DiscoverCallback
import com.android.vitrinova.presentation.detail.DetailFragment
import com.android.vitrinova.utility.LiveData.EventObserver
import com.android.vitrinova.utility.constants.BundleConstants
import com.android.vitrinova.utility.enums.DiscoverTypes
import com.android.vitrinova.utility.extension.vertical
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class DiscoverFragment : BaseFragment<DiscoverVM, FragmentDiscoverBinding>(),
    DiscoverCallback {

    override val layoutRes = R.layout.fragment_discover
    override fun getViewModelClass() = DiscoverVM::class.java


    private val adapter by lazy { DiscoverAdapter(this) }


    override fun initUI() {
        super.initUI()


        binding.mainRv.vertical(adapter)


        viewModel.setStateEvent(DiscoveryStateEvent.FetchData)

    }

    override fun subscribeObservers() {
        super.subscribeObservers()

        viewModel.discover.observe(viewLifecycleOwner, EventObserver {
            it.handle(success = { items ->
                adapter.items = items
            })
            binding.swipeRefreshLayout.isRefreshing = false
        })

    }

    override fun clickListeners() {
        super.clickListeners()

        binding.swipeRefreshLayout.setOnRefreshListener {
            adapter.items = emptyList()
            viewModel.setStateEvent(DiscoveryStateEvent.FetchData)
        }

    }


    override fun clickAllItem(postType: String) {
        val bundle = Bundle()

        val items = adapter.items.find { it.type == postType }


        bundle.apply {
            putString(BundleConstants.POST_TYPE, postType)
            putString(BundleConstants.TITLE, items?.title ?: "")

            putParcelableArrayList(
                BundleConstants.ITEMS, when (postType) {
                    DiscoverTypes.NEW_PRODUCTS.value -> {
                        items?.products
                    }
                    DiscoverTypes.COLLECTIONS.value -> {
                        items?.collections
                    }
                    DiscoverTypes.EDITOR_SHOPS.value, DiscoverTypes.NEW_SHOPS.value -> {
                        items?.shops
                    }
                    else -> {
                        items?.products
                    }
                }
            )

        }
        /***
         * Bütün ürünlerin gösterildiği taraf benzer olduğu için hepsini aynı sayfada yönetmek istedim
         * bu sayede ortak bir değişiklik yapıldığı zaman tek bir yerden halledilebilecek.
         * */
        Navigator.find<MainController>(DetailFragment.newInstance(bundle)).navigate()
    }

    companion object {
        fun newInstance() = DiscoverFragment()
    }


}
