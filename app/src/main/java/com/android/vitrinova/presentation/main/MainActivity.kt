package com.android.vitrinova.presentation.main

import android.app.SearchManager
import android.content.Intent
import com.android.easynav.src.Navigator
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseActivity
import com.android.vitrinova.core.MainController
import com.android.vitrinova.databinding.ActivityMainBinding
import com.android.vitrinova.presentation.splash.SplashFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class MainActivity : BaseActivity<MainVM, ActivityMainBinding>() {

    override val layoutRes = R.layout.activity_main
    override fun getViewModelClass() = MainVM::class.java


    override fun initUI() {
        /**
         * Uygulamarda esneklik ve kolaylık açısından One Activity Multiple Fragment yapısını tercih ediyorum
         * Bu yapı için  Android Navigation Component kütüphanesi tercih edilebilir
         * Fakat ben burada kendi geliştirdiğim fragment controller kütüphanesini kullanıyorum
         *
         * https://github.com/kemaltunc/EasyController
         *
         * */
        Navigator.bind<MainController>(this, R.id.main_container, SplashFragment.newInstance()) {
            history = false
        }
    }


}