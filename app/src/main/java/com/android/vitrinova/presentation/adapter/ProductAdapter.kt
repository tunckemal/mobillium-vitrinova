package com.android.vitrinova.presentation.adapter

import android.view.ViewGroup
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseAdapter
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.Product
import com.android.vitrinova.databinding.CellProductBinding
import com.android.vitrinova.utility.extension.centerLine


class ProductAdapter(private val fullscreen: Boolean = false) : BaseAdapter<Product>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Product, *> {
        return ProductViewHolder(parent, fullscreen)
    }

    class ProductViewHolder(parent: ViewGroup, private val fullscreen: Boolean) :
        BaseViewHolder<Product, CellProductBinding>(parent, R.layout.cell_product) {

        override fun bind(item: Product, binding: CellProductBinding) {
            with(binding) {
                data = item
                executePendingBindings()

                cellProductOldPrice.centerLine()

                if (fullscreen) {
                    binding.root.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
                }
            }


        }

    }
}