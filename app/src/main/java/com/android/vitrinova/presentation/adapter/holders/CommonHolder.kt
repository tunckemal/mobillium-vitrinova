package com.android.vitrinova.presentation.adapter.holders

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.databinding.CellCommonHolderBinding
import com.android.vitrinova.presentation.adapter.CollectionAdapter
import com.android.vitrinova.presentation.adapter.ProductAdapter
import com.android.vitrinova.presentation.adapter.ShopAdapter
import com.android.vitrinova.presentation.callbacks.DiscoverCallback
import com.android.vitrinova.utility.enums.DiscoverTypes
import com.android.vitrinova.utility.extension.horizantal

class CommonHolder(
    parent: ViewGroup,
    private val discoverCallback: DiscoverCallback,
    private val viewPool: RecyclerView.RecycledViewPool
) : BaseViewHolder<MainDiscoverResponse, CellCommonHolderBinding>(
    parent,
    R.layout.cell_common_holder
) {
    private val productAdapter by lazy { ProductAdapter() }
    private val collectionAdapter by lazy { CollectionAdapter() }
    private val shopAdapter by lazy { ShopAdapter() }
    override fun bind(item: MainDiscoverResponse, binding: CellCommonHolderBinding) {

        with(binding) {
            data = item
            executePendingBindings()


            when (item.type) {
                DiscoverTypes.NEW_PRODUCTS.value -> {
                    cellMainProductRv.recyclerView.horizantal(productAdapter)
                    productAdapter.items = item.products ?: emptyList()
                }

                DiscoverTypes.COLLECTIONS.value -> {
                    cellMainProductRv.recyclerView.horizantal(collectionAdapter)
                    collectionAdapter.items = item.collections ?: emptyList()
                }

                DiscoverTypes.NEW_SHOPS.value -> {
                    cellMainProductRv.recyclerView.horizantal(
                        shopAdapter,
                        snapHelper = androidx.recyclerview.widget.LinearSnapHelper()
                    )
                    shopAdapter.items = item.shops ?: emptyList()
                }
            }

            cellMainProductRv.apply {
                setText(item.title ?: "")
                allItemClickListener {
                    discoverCallback.clickAllItem(item.type ?: "")
                }

           //     recyclerView.setRecycledViewPool(viewPool)

            }


        }

    }


}