package com.android.vitrinova.presentation.adapter

import android.view.ViewGroup
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseAdapter
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.Shop
import com.android.vitrinova.databinding.CellEditorBinding
import com.android.vitrinova.utility.extension.horizantal

class EditorAdapter(private val fullscreen: Boolean = false) : BaseAdapter<Shop>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder<Shop, *> {
        return EditorViewHolder(parent,fullscreen)
    }


    class EditorViewHolder(parent: ViewGroup, private val fullscreen: Boolean = false) :
        BaseViewHolder<Shop, CellEditorBinding>(parent, R.layout.cell_editor) {

        private val adapter by lazy { PopularAdapter() }
        override fun bind(item: Shop, binding: CellEditorBinding) {
            with(binding) {
                data = item

                executePendingBindings()

                cellEditorInnerRv.horizantal(adapter)

                adapter.items = item.popular_products ?: emptyList()
            }

            if (fullscreen) {
                binding.root.layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
            }
        }

    }
}