package com.android.vitrinova.presentation.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.vitrinova.core.BaseAdapter
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.presentation.adapter.holders.CategoryMainHolder
import com.android.vitrinova.presentation.adapter.holders.CommonHolder
import com.android.vitrinova.presentation.adapter.holders.EditorMainHolder
import com.android.vitrinova.presentation.adapter.holders.SliderHolder
import com.android.vitrinova.presentation.callbacks.DiscoverCallback
import com.android.vitrinova.utility.enums.DiscoverTypes

class DiscoverAdapter(private val discoverCallback: DiscoverCallback) :
    BaseAdapter<MainDiscoverResponse>() {


    val viewPool = RecyclerView.RecycledViewPool()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<MainDiscoverResponse, *> {
        return when (viewType) {
            DiscoverTypes.FEATURED.type -> {
                SliderHolder(parent, discoverCallback)
            }
            DiscoverTypes.NEW_PRODUCTS.type, DiscoverTypes.COLLECTIONS.type, DiscoverTypes.NEW_SHOPS.type -> {
                CommonHolder(parent, discoverCallback, viewPool)
            }
            DiscoverTypes.CATEGORIES.type -> {
                CategoryMainHolder(parent, discoverCallback, viewPool)
            }
            DiscoverTypes.EDITOR_SHOPS.type -> {
                EditorMainHolder(parent, discoverCallback, viewPool)
            }
            else -> {
                CommonHolder(parent, discoverCallback, viewPool)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        val map = DiscoverTypes.values().associateBy(DiscoverTypes::value)

        fun from(value: String) = map[value]

        val discoverTypes = from(items.getOrNull(position)?.type ?: "")

        return discoverTypes?.type ?: 0
    }


}