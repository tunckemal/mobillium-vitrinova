package com.android.vitrinova.presentation.adapter.holders

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseViewHolder
import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.databinding.CellMainCategoryBinding
import com.android.vitrinova.presentation.adapter.CategoryAdapter
import com.android.vitrinova.presentation.callbacks.DiscoverCallback
import com.android.vitrinova.utility.extension.horizantal


class CategoryMainHolder(
    parent: ViewGroup,
    private val discoverCallback: DiscoverCallback,
    private val viewPool: RecyclerView.RecycledViewPool
) : BaseViewHolder<MainDiscoverResponse, CellMainCategoryBinding>(
    parent,
    R.layout.cell_main_category
) {
    private val adapter by lazy { CategoryAdapter() }
    override fun bind(item: MainDiscoverResponse, binding: CellMainCategoryBinding) {

        with(binding) {
            data = item
            executePendingBindings()

            cellCategoryRv.apply {
                setText(item.title ?: "")
                recyclerView.horizantal(adapter)

                allItemClickListener {
                    discoverCallback.clickAllItem(item.type ?: "")
                }

               // recyclerView.setRecycledViewPool(viewPool)
            }

            adapter.items = item.categories ?: emptyList()
        }


    }


}