package com.android.vitrinova.presentation.tab

import android.app.Activity.RESULT_OK
import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.speech.RecognizerIntent
import android.view.Menu
import android.view.MenuInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import com.android.easynav.src.Navigator
import com.android.vitrinova.R
import com.android.vitrinova.core.BaseFragment
import com.android.vitrinova.core.ChildController
import com.android.vitrinova.databinding.FragmentTabBinding
import com.android.vitrinova.presentation.discovery.DiscoverFragment
import com.android.vitrinova.utility.extension.gone
import com.android.vitrinova.utility.extension.show
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@AndroidEntryPoint
@ExperimentalCoroutinesApi
class TabFragment : BaseFragment<TabVM, FragmentTabBinding>() {

    override val layoutRes = R.layout.fragment_tab
    override fun getViewModelClass() = TabVM::class.java

    private val REQUEST_VOICE_CODE: Int = 1234

    private var searchView: SearchView? = null



    private val discoverFragment by lazy { DiscoverFragment.newInstance() }

    override fun initUI() {
        super.initUI()

        initToolbar()


        binding.tabBottomNavigation.selectedItemId = R.id.menu_discover

        /***
         * TabFragment içerisindeki FrameLayout içinde fragment açmak için
         * ChildController adında bir controller oluşturulup, addController fonksiyonu
         * ile eklendi
         *
         * https://github.com/kemaltunc/EasyController
         *
         * */
        Navigator.find<ChildController>(discoverFragment).apply {
            addController(R.id.tab_container, childFragmentManager)

            navigate()
        }


    }

    override fun subscribeObservers() {
        super.subscribeObservers()

    }


    private fun initToolbar() {
        (activity as AppCompatActivity).apply {
            setSupportActionBar(binding.tabHeader.tabToolbar)
            supportActionBar?.setDisplayShowTitleEnabled(false)
        }

        binding.tabHeader.tabToolbar.setNavigationIcon(R.drawable.ic_menu)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_menu, menu)

        searchView = menu.findItem(R.id.search_view).actionView as SearchView

        val searchManager = activity?.getSystemService(Context.SEARCH_SERVICE) as SearchManager


        menu.findItem(R.id.search_view).setOnMenuItemClickListener {
            startVoiceRecognitionActivity()
            return@setOnMenuItemClickListener true
        }

        searchView?.apply {
            setSearchableInfo(searchManager.getSearchableInfo(activity?.componentName))
            setOnQueryTextFocusChangeListener { view, b ->
                if (b) {
                    binding.tabHeader.searchTv.gone()
                } else {
                    binding.tabHeader.searchTv.show()
                }
            }
        }

        super.onCreateOptionsMenu(menu, inflater)
    }


    private fun startVoiceRecognitionActivity() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
        intent.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL,
            RecognizerIntent.LANGUAGE_MODEL_FREE_FORM
        )
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT, getString(R.string.information_listening))
        startActivityForResult(intent, REQUEST_VOICE_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == REQUEST_VOICE_CODE && resultCode == RESULT_OK) {
            val matches: ArrayList<String> =
                data?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS) as ArrayList<String>

            if (matches.isNotEmpty()) {
                val query = matches[0]
                searchView?.setQuery(query, false)
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        fun newInstance() = TabFragment()
    }


}
