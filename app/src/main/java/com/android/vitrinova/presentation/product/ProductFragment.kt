package com.android.vitrinova.presentation.product

import com.android.vitrinova.R
import com.android.vitrinova.core.BaseFragment
import com.android.vitrinova.databinding.FragmentProductBinding
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ProductFragment : BaseFragment<ProductVM, FragmentProductBinding>() {

    override val layoutRes = R.layout.fragment_product
    override fun getViewModelClass() = ProductVM::class.java


    override fun initUI() {
        super.initUI()


    }

    override fun subscribeObservers() {
        super.subscribeObservers()

    }

    companion object {
        fun newInstance() = ProductFragment()
    }


}
