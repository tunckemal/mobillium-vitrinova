package com.android.vitrinova.di

import android.app.Application
import android.content.Context
import com.android.vitrinova.BuildConfig
import com.android.vitrinova.data.source.network.service.DiscoverService
import com.android.vitrinova.utility.NetworkUtil.CACHE_SIZE
import com.android.vitrinova.utility.NetworkUtil.CONNECT_TIME_OUT
import com.android.vitrinova.utility.NetworkUtil.READ_TIME_OUT
import com.android.vitrinova.utility.NetworkUtil.WRITE_TIME_OUT
import com.android.vitrinova.utility.NetworkUtil.retrofitHeader
import com.android.vitrinova.utility.NetworkUtil.retrofitLogging
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.readystatesoftware.chuck.ChuckInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class NetworkModule {
    @Singleton
    @Provides
    fun provideOkHttpClient(
        application: Application,
        @ApplicationContext context: Context
    ): OkHttpClient =
        OkHttpClient.Builder()
            .connectTimeout(CONNECT_TIME_OUT, TimeUnit.SECONDS)
            .readTimeout(READ_TIME_OUT, TimeUnit.SECONDS)
            .writeTimeout(WRITE_TIME_OUT, TimeUnit.SECONDS)
            .cache(Cache(context.cacheDir, CACHE_SIZE))
            .addInterceptor(retrofitHeader(context))
            .addInterceptor(retrofitLogging())
            .addNetworkInterceptor(StethoInterceptor())
            .addNetworkInterceptor(ChuckInterceptor(application))
            .build()

    @Provides
    @Singleton
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {

        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create()).build()
    }


    @Provides
    @Singleton
    fun provideDiscoveryService(retrofit: Retrofit): DiscoverService {
        return retrofit.create(DiscoverService::class.java)
    }


}

