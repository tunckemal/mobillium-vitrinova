package com.android.vitrinova.di


import com.android.vitrinova.data.repository.DiscoverRepositoryImpl
import com.android.vitrinova.data.source.network.service.DiscoverService
import com.android.vitrinova.domain.repository.DiscoverRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import javax.inject.Singleton


@Module
@InstallIn(ApplicationComponent::class)
class RepositoryModule {

    @Singleton
    @Provides
    fun provideDiscoveryRepository(
        discoveryService: DiscoverService
    ): DiscoverRepository {
        return DiscoverRepositoryImpl(discoveryService)
    }
}