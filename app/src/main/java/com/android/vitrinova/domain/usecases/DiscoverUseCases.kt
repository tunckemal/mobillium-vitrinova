package com.android.vitrinova.domain.usecases

import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.domain.repository.DiscoverRepository
import javax.inject.Inject

class DiscoverUseCases @Inject constructor(private val repository: DiscoverRepository) {

    suspend fun getDiscover(): ArrayList<MainDiscoverResponse>  {
        return repository.getDiscovery()
    }

}