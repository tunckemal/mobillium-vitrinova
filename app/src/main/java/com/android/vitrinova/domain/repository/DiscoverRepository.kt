package com.android.vitrinova.domain.repository

import com.android.vitrinova.data.model.response.MainDiscoverResponse

interface DiscoverRepository {
    suspend fun getDiscovery(): ArrayList<MainDiscoverResponse>
}