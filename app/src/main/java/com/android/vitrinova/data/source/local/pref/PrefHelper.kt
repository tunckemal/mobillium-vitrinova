package com.android.vitrinova.data.source.local.pref

interface PrefHelper {
    fun saveAuthorizationToken(authKey: String?)
    fun getAuthorizationToken(): String?

    fun saveRefreshAuthorizationToken(refreshKey: String?)
    fun getRefreshAuthorizationToken(): String?

    fun clear()
}