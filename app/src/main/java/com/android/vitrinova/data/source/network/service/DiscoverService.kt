package com.android.vitrinova.data.source.network.service

import com.android.vitrinova.data.model.response.MainDiscoverResponse
import retrofit2.http.GET

interface DiscoverService {

    @GET("discover")
    suspend fun getDiscover(): ArrayList<MainDiscoverResponse>
}