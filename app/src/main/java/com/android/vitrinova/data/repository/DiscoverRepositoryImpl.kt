package com.android.vitrinova.data.repository

import com.android.vitrinova.data.model.response.MainDiscoverResponse
import com.android.vitrinova.data.source.network.service.DiscoverService
import com.android.vitrinova.domain.repository.DiscoverRepository

class DiscoverRepositoryImpl(
    private val discoveryService: DiscoverService
) : DiscoverRepository {

    override suspend fun getDiscovery(): ArrayList<MainDiscoverResponse> {
        return discoveryService.getDiscover()
    }

}