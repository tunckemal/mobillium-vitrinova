package com.android.vitrinova.data.source.local.pref


import android.content.Context
import android.content.SharedPreferences
import javax.inject.Inject

class PrefHelperImpl @Inject constructor(context: Context) : PrefHelper {

    private var mPrefs: SharedPreferences =
        context.getSharedPreferences(PREFHELPER, Context.MODE_PRIVATE)


    override fun saveAuthorizationToken(authKey: String?) {
        mPrefs.edit().putString(AUTHORIZATON_PREF_KEY, "Bearer $authKey").apply()
    }

    override fun getAuthorizationToken(): String? {
        return mPrefs.getString(AUTHORIZATON_PREF_KEY, "")
    }

    override fun saveRefreshAuthorizationToken(refreshKey: String?) {
        mPrefs.edit().putString(REFRESH_AUTHORIZATON_PREF_KEY, refreshKey).apply()
    }

    override fun getRefreshAuthorizationToken(): String? {
        return mPrefs.getString(REFRESH_AUTHORIZATON_PREF_KEY, "")
    }

    override fun clear() {
        mPrefs.edit().clear().apply()
    }

    companion object {
        const val PREFHELPER = "Pref"
        const val AUTHORIZATON_PREF_KEY = "authorizationPref"
        const val REFRESH_AUTHORIZATON_PREF_KEY = "refreshauthorizationPref"
    }
}