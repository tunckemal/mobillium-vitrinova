package com.android.vitrinova.data.model.response


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Shop(
    @SerializedName("id")
    var id: Int? = null,
    @SerializedName("name")
    var name: String? = null,
    @SerializedName("slug")
    var slug: String? = null,
    @SerializedName("definition")
    var definition: String? = null,
    @SerializedName("name_updateable")
    var nameUpdateable: Boolean? = null,
    @SerializedName("vacation_mode")
    var vacationMode: Int? = null,
    @SerializedName("created_at")
    var createdAt: String? = null,
    @SerializedName("shop_payment_id")
    var shopPaymentId: Int? = null,
    @SerializedName("product_count")
    var productCount: Int? = null,
    @SerializedName("shop_rate")
    var shopRate: Int? = null,
    @SerializedName("comment_count")
    var commentCount: Int? = null,
    @SerializedName("follower_count")
    var followerCount: Int? = null,
    @SerializedName("is_editor_choice")
    var isEditorChoice: Boolean? = null,
    @SerializedName("is_following")
    var isFollowing: Boolean? = null,
    @SerializedName("cover")
    var cover: Cover? = null,
    @SerializedName("share_url")
    var shareUrl: String? = null,
    @SerializedName("logo")
    var logo: Logo? = null,
    @SerializedName("popular_products")
    var popular_products: ArrayList<PopularProduct>? = null
):Parcelable